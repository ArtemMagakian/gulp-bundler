let project_folder = 'dist',
    src_folder = '#src',
    fs = require('fs');

let path = {
    build: {
        html: project_folder + '/',
        css: project_folder + '/css/',
        js: project_folder + '/js/',
        img: project_folder + '/img/',
        fonts: project_folder + '/fonts/'
    },
    src: {
        html: [src_folder + '/*.html', '!' + src_folder + '/_*.html'],
        pug: [src_folder + '/*.pug', '!' + src_folder + '/_*.pug'],
        css: [src_folder + '/scss/all.scss', '!' + src_folder + '/_*.scss'],
        js: src_folder + '/js/all.js',
        img: src_folder + '/img/**/*.{jpg, png, svg, gif, ico, webp}',
        fonts: src_folder + '/fonts/*.ttf'
    },
    watch: {
        html: src_folder + '/**/*.html',
        pug: src_folder + '/**/*.pug',
        css: src_folder + '/**/*.scss',
        js: src_folder + '/js/**/*.js',
        img: src_folder + '/img/**/*.{jpg, png, svg, gif, ico, webp}'
    },
    clean: './' + project_folder + '/'
};

let { src, dest } = require('gulp'),
    gulp = require('gulp'),
    browsersync = require('browser-sync').create(),
    fileinclude = require('gulp-file-include'),
    del = require('del'),
    scss = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    group_media = require('gulp-group-css-media-queries'),
    clean_css = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify-es').default,
    imagemin = require('gulp-imagemin'),
    webp = require('gulp-webp'),
    webphtml = require('gulp-webp-html'),
    webpcss = require('gulp-webpcss'),
    svgSprite = require('gulp-svg-sprite'),
    ttf2woff = require('gulp-ttf2woff'),
    ttf2woff2 = require('gulp-ttf2woff2'),
    babel = require('gulp-babel'),
    pug = require('gulp-pug'),
    rigger = require('gulp-rigger'),
    fonter = require('gulp-fonter');

function browserSync(params) {
    browsersync.init({
        server: {
            baseDir: './' + project_folder + '/'
        },
        port: 3000,
        notify: false
    });
}

function html() {
    return src(path.src.html)
        .pipe( fileinclude() )
        .pipe( webphtml() )
        .pipe( dest(path.build.html) )
        .pipe( browsersync.stream() );
}

// task for pug
function pugToHtml(){
    return src(path.src.pug)
        .pipe( fileinclude() )
        .pipe( pug({
            pretty: true
        }) )
        .pipe( dest(path.build.html) )
        .pipe( browsersync.stream() );
}

function css() {
    return src(path.src.css)
        .pipe( rigger() ) //Прогоним через rigger
        .pipe(
            scss({
                outputStyle: 'expanded'
            })
        )
        .pipe( group_media() )
        .pipe(
            autoprefixer({
                overrideBrowserslist: ['last 5 versions'],
                cascade: true
            })
        )
        .pipe( webpcss({webpClass: '.webp',noWebpClass: '.no-webp'}) )
        .pipe( dest(path.build.css) )
        .pipe( clean_css() )
        .pipe(
            rename({
                extname: '.min.css'
            })
        )
        .pipe( dest(path.build.css) )
        .pipe( browsersync.stream() );
}

function js() {
    return src(path.src.js) //Найдем наш main файл
        .pipe( rigger() ) //Прогоним через rigger
        .pipe( babel() ) //Прогоним через транспайлер, переписывающий код на ES6 в код на предыдущем стандарте ES5
        .pipe( uglify() ) //Сожмем наш js
        .pipe(
            rename({
                extname: '.min.js'
            })
        )
        .pipe( dest(path.build.js) ) //Выплюнем готовый файл в build
        .pipe( browsersync.stream() ); //И перезагрузим сервер
}

function images() {
    return src(path.src.img)
        .pipe(
            webp({
                quality: 70
            })
        )
        .pipe( dest(path.build.img) )
        .pipe( src(path.src.img) )
        .pipe(
            imagemin({
                progressive: true,
                svgoPlugins: [{ removeViewBox: false }],
                interlaced: true,
                optimizationLevel: 3 // 0 to 7
            })
        )
        .pipe( dest(path.build.img) )
        .pipe( browsersync.stream() );
}

function fonts(params) {
    src(path.src.fonts)
        .pipe( ttf2woff() )
        .pipe( dest(path.build.fonts) );
    return src(path.src.fonts)
        .pipe( ttf2woff2() )
        .pipe( dest(path.build.fonts) );
}

gulp.task('otf2ttf', function() {
    return src([src_folder + '/fonts/*.otf'])
        .pipe(fonter({
            formats: ['ttf']
        }))
        .pipe( dest(src_folder + '/fonts/') );
});

gulp.task('svgSprite', function() {
    return gulp.src([src_folder + '/iconsprite/*.svg'])
        .pipe( svgSprite({
            mode: {
                stack: {
                    sprite: '../icons/icons.svg', // sprite file name
                    example: true
                }
            },
        }) )
        .pipe( dest(path.build.img) );
});

function fontsStyle(params) {
    let file_content = fs.readFileSync(src_folder + '/scss/misc/_fonts.scss');

    if (file_content == '') {
        fs.writeFile(src_folder + '/scss/misc/_fonts.scss', '', cb);

        return fs.readdir(path.build.fonts, function (err, items) {
            if (items) {
                let c_fontname;

                for (var i = 0; i < items.length; i++) {
                    let fontname = items[i].split('.');
                    fontname = fontname[0];

                    if (c_fontname != fontname) {
                        fs.appendFile(src_folder + '/scss/misc/fonts.scss', '@include font("' + fontname + '", "' + fontname + '", "400", "normal");\r\n', cb);
                    }
                    c_fontname = fontname;
                }
            }
        });
    }
}

function watchFiles(params) {
    gulp.watch([path.watch.html], html);
    gulp.watch([path.watch.pug], pugToHtml);
    gulp.watch([path.watch.css], css);
    gulp.watch([path.watch.js], js);
    gulp.watch([path.watch.img], images);
}

function clean(params) {
    return del(path.clean);
}

let build = gulp.series(clean, gulp.parallel(fonts, html, pugToHtml, css, js, images), fontsStyle);
let watch = gulp.parallel(build, watchFiles, browserSync);

exports.fontsStyle = fontsStyle;
exports.fonts = fonts;
exports.html = html;
exports.pugToHtml = pugToHtml;
exports.css = css;
exports.js = js;
exports.images = images;
exports.build = build;
exports.watch = watch;
exports.default = watch;